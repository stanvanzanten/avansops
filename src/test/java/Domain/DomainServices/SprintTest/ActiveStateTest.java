package Domain.DomainServices.SprintTest;

import Domain.DomainModel.Notification.NotificationType;
import Domain.DomainModel.Pipeline.Packages.Packages;
import Domain.DomainModel.Pipeline.Pipeline;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Sprint.ActiveState;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ActiveStateTest {

    @Test
    void fromActiveToFinished() {
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);

        sprint.setState(new ActiveState(sprint));

        sprint.setFinished();

        assertEquals("finished", sprint.getState().getTitle());
    }

    @Test
    void fromActiveToInit(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);

        sprint.setState(new ActiveState(sprint));

        sprint.setInit();

        assertEquals("active", sprint.getState().getTitle());
    }
}
