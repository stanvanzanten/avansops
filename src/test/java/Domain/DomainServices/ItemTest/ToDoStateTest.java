package Domain.DomainServices.ItemTest;

import Domain.DomainModel.Notification.NotificationType;
import Domain.DomainModel.Pipeline.Packages.Packages;
import Domain.DomainModel.Pipeline.Pipeline;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Item.BacklogItem;
import Domain.DomainServices.Item.ItemState.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;


class ToDoStateTest {
    @Test
    void fromToDoStateToDoingState(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);
        BacklogItem backlogItem1 = new BacklogItem("Make something", "I will try to make something here...", user, sprint);

        backlogItem1.setState(new ToDoState(backlogItem1));

        backlogItem1.setDoing();

        assertEquals("doing", backlogItem1.getState().getTitle());
    }

    @Test
    void goToAnyOtherState(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);
        BacklogItem backlogItem1 = new BacklogItem("Make something", "I will try to make something here...", user, sprint);

        backlogItem1.setState(new ToDoState(backlogItem1));
        //Now skip the doing and try to set anything else.

        backlogItem1.setTested();
        backlogItem1.setDone();
        backlogItem1.setTesting();
        backlogItem1.setReady();

        assertEquals("todo", backlogItem1.getState().getTitle());
    }
}
