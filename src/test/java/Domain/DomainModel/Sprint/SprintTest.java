package Domain.DomainModel.Sprint;

import Domain.DomainModel.Notification.NotificationType;
import Domain.DomainModel.Pipeline.Packages.Packages;
import Domain.DomainModel.Pipeline.Pipeline;
import Domain.DomainModel.Project.Project;
import Domain.DomainModel.Repository.Repository;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Item.BacklogItem;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class SprintTest {

    @Test
    void addSprintToProject(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);

        project.addSprint(sprint);
        assertTrue(project.getSprint().contains(sprint));
    }

    @Test
    void isScrummasterAdded(){
        User scrummaster = new User("scrummaster", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User tester = new User("tester", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", scrummaster, tester, pipeline, false);

        assertEquals(sprint.getScrumMaster(), scrummaster);
    }

    @Test
    void isTesterAdded(){
        User scrummaster = new User("scrummaster", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User tester = new User("tester", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", scrummaster, tester, pipeline, false);


        assertEquals(sprint.getTester(), tester);
    }

    @Test
    void addDevelopers(){
        User scrummaster = new User("scrummaster", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User tester = new User("tester", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User dev1 = new User("dev1", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User dev2 = new User("dev2", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User dev3 = new User("dev3", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User dev4 = new User("dev4", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", scrummaster, tester, pipeline, false);

        sprint.addDeveloper(dev1);
        sprint.addDeveloper(dev2);
        sprint.addDeveloper(dev3);
        sprint.addDeveloper(dev4);


        assertEquals(sprint.getDevelopers().size(), 4);
        assertTrue(sprint.getDevelopers().contains(dev1));
    }

    @Test
    void addBacklogFromActiveState(){
        User scrummaster = new User("scrummaster", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User tester = new User("tester", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);

        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);

        Sprint sprint = new Sprint("sprint1", scrummaster, tester, pipeline, false);
        BacklogItem backlogItem1 = new BacklogItem("Make something", "I will try to make something here...", tester, sprint);

        sprint.setInit();
        //This one will be added to prove it works.
        sprint.addBacklogItem(backlogItem1);
        sprint.setActive();
        //This one won't be added because the sprint is active.
        sprint.addBacklogItem(backlogItem1);

        assertEquals(sprint.getBacklogItems().size(), 1);
    }

    @Test
    void whenFinishedReturnFinishedState(){
        User scrummaster = new User("scrummaster", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User tester = new User("tester", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);

        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);

        Sprint sprint = new Sprint("sprint1", scrummaster, tester, pipeline, false);

        sprint.setInit();
        sprint.setActive();
        sprint.setFinished();

        assertEquals(sprint.getState().getTitle(), "finished");
    }

    @Test
    void startReviewAfterFinish(){
        User scrummaster = new User("scrummaster", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User tester = new User("tester", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);

        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("dotnet", "selenium", "azure", "codesonar" , packages);

        Sprint sprint = new Sprint("sprint1", scrummaster, tester, pipeline, false);

        sprint.setInit();
        sprint.setActive();
        sprint.setFinished();
        sprint.startPipeline();

        assertEquals(sprint.getReviewed(), true);
    }

    @Test
    void closeSprintAfterRelease(){
        User scrummaster = new User("scrummaster", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        User tester = new User("tester", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);

        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("dotnet", "selenium", "azure", "codesonar" , packages);
        Repository repo = new Repository();
        pipeline.setSource(repo);
        Sprint sprint = new Sprint("sprint1", scrummaster, tester, pipeline, true);

        sprint.setInit();
        sprint.setActive();
        sprint.setFinished();
        sprint.startPipeline();

        //Try to set it back to active
        sprint.setActive();

        //Check if the sprint has been closed.
        assertTrue(sprint.getReleased());
        //Check if the sprint is still in the same state as when it closed.
        assertEquals(sprint.getState().getTitle(), "finished");
    }
}