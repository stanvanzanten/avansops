package Domain.DomainModel.Forum;

import Domain.DomainModel.Forum.Discussion;
import Domain.DomainModel.Forum.Forum;
import Domain.DomainModel.Forum.Message;
import Domain.DomainModel.Notification.NotificationType;
import Domain.DomainModel.Pipeline.Packages.Packages;
import Domain.DomainModel.Pipeline.Pipeline;
import Domain.DomainModel.Project.Project;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Item.BacklogItem;
import Domain.DomainServices.Item.ItemState.DoingState;
import Domain.DomainServices.Item.ItemState.DoneState;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ForumTest {
    @Test
    void addForumToBacklogItem(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);
        BacklogItem backlogItem = new BacklogItem("backlogitem1", "testcontent", user, sprint);
        Discussion discussion = new Discussion(backlogItem, user, "testdescription");
        Discussion discussion2 = new Discussion(backlogItem, user, "testdescription");
        Forum forum = project.getForum();

        forum.addDiscussion(discussion);
        forum.addDiscussion(discussion2);



        assertTrue(forum.getDiscussions().contains(discussion));
        assertEquals(forum.getDiscussions().size(), 2);
    }

    @Test
    void discussionHasDescription(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);
        BacklogItem backlogItem = new BacklogItem("backlogitem1", "testcontent", user, sprint);

        Discussion discussion = new Discussion(backlogItem, user, "testdescription");

        assertEquals(discussion.getDescription(), "testdescription");
    }

    @Test
    void addCommentToDiscussion(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);
        BacklogItem backlogItem = new BacklogItem("backlogitem1", "testcontent", user, sprint);
        Discussion discussion = new Discussion(backlogItem, user, "testdescription");
        Message message = new Message(user, "testmessage");
        Forum forum = project.getForum();

        forum.addDiscussion(discussion);

        discussion.addMessage(message);



        assertTrue(discussion.getMessages().contains(message));
        assertEquals(discussion.getMessages().get(0).getText(), "testmessage");
    }

    @Test
    void discussionIsBoundToBacklogItem(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);
        BacklogItem backlogItem = new BacklogItem("backlogitem1", "testcontent", user, sprint);
        Discussion discussion = new Discussion(backlogItem, user, "testdescription");
        Forum forum = project.getForum();

        forum.addDiscussion(discussion);

        assertEquals(discussion.getBacklogItem(), backlogItem);
    }

    @Test
    void shouldNotPostReactionIfBacklogItemStateIsDone(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);
        BacklogItem backlogItem = new BacklogItem("backlogitem1", "testcontent", user, sprint);
        Discussion discussion = new Discussion(backlogItem, user, "testdescription");
        Message message = new Message(user, "testmessage");
        Forum forum = project.getForum();

        backlogItem.setState(new DoneState(backlogItem));
        forum.addDiscussion(discussion);

        discussion.addMessage(message);

        assertEquals(discussion.getMessages().size(), 0);
    }





}
