package Domain.DomainModel.Project;

import Domain.DomainModel.Notification.NotificationType;
import Domain.DomainModel.Project.Project;
import Domain.DomainModel.User.User;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProjectTest {
    @Test
    void createProjectWithTitle(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");

        Assert.assertEquals(project.getProjectname(), "testproject");
    }

    @Test
    void createProjectWithOneOwner(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");

        Assert.assertEquals(project.getProductOwner(), user);
    }

}