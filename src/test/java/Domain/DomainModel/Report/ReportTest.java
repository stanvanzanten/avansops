package Domain.DomainModel.Report;

import Domain.DomainServices.Report.ConsoleReport;
import Domain.DomainServices.Report.Footer;
import Domain.DomainServices.Report.Header;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;


public class ReportTest {
    
    @Test
    void adjustHeaderAndFooter(){

        Header predefinedHeader = new Header("logo", "name", "PrintTest");
        Footer predefinedFooter = new Footer(LocalDate.now(), "1.0");
        ConsoleReport consoleReport = new ConsoleReport(predefinedHeader, predefinedFooter);
        
        Header newHeader = new Header("new Logo", "new Company", "new Project");
        Footer newFooter = new Footer(LocalDate.now().plusDays(2),"2.0");

        consoleReport.setHeader(newHeader);
        consoleReport.setFooter(newFooter);

        boolean sameHeader = false;
        boolean sameFooter = false;
        
        if(consoleReport.getHeader() == predefinedHeader){
            sameHeader = true;
        }
        if(consoleReport.getFooter() == predefinedFooter){
            sameFooter = true;
        }

        //If the headers and footers are different, the test will pass.
        assertFalse(sameHeader);
        assertFalse(sameFooter);
    }
}
