package Domain.DomainModel.Item;

import Domain.DomainModel.Notification.NotificationType;
import Domain.DomainModel.Pipeline.Packages.Packages;
import Domain.DomainModel.Pipeline.Pipeline;
import Domain.DomainModel.Project.Project;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Item.Activity;
import Domain.DomainServices.Item.BacklogItem;
import Domain.DomainServices.Sprint.FinishedState;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    @Test
    void addActivityToBacklogItem(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);

        BacklogItem backlogItem = new BacklogItem("backlogitem1", "testcontent", user, sprint);
        Activity activity = new Activity("activity", "testcontent", user, sprint);
        Activity activity1 = new Activity("activity1", "testcontent", user, sprint);

        backlogItem.addActivity(activity);
        backlogItem.addActivity(activity1);

        assertEquals(backlogItem.getActivities().size(), 2);
        assertTrue(backlogItem.getActivities().contains(activity));
    }

    @Test
    void addBackLogItemToSprint(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);

        BacklogItem backlogItem = new BacklogItem("backlogitem1", "testcontent", user, sprint);
        sprint.addBacklogItem(backlogItem);

        assertTrue(sprint.getBacklogItems().contains(backlogItem));
    }

    @Test
    void addBackLogItemToSprintFail(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);

        BacklogItem backlogItem = new BacklogItem("backlogitem1", "testcontent", user, sprint);
        sprint.setState(new FinishedState(sprint));
        sprint.addBacklogItem(backlogItem);

        assertFalse(sprint.getBacklogItems().contains(backlogItem));
    }

    @Test
    void backlogItemCanOnlyHaveOneDeveloper(){
        User user = new User("testname", "test@hotmail.com", "06666666", "testpw", NotificationType.EMAIL);
        Project project = new Project(user, "testproject");
        ArrayList<Packages> packages = new ArrayList<Packages>();
        Pipeline pipeline = new Pipeline("test", "test", "test", "test" , packages);
        Sprint sprint = new Sprint("sprint1", user, user, pipeline, false);

        BacklogItem backlogItem = new BacklogItem("backlogitem1", "testcontent", user, sprint);



        assertEquals(backlogItem.getDeveloper(), user);
    }
}

