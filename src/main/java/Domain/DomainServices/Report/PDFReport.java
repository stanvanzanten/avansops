package Domain.DomainServices.Report;

import Domain.DomainModel.Report.Report;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;

import java.io.*;

public class PDFReport extends Report {
    public PDFReport(Header header, Footer footer) {
        super(header, footer);
    }

    public void printSprint(Sprint sprint){
        printToFile(sprintAttributes(sprint));
    }

    public void printToFile(String string){
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("pdfreport - Sprint.pdf"), "utf-8"));
            writer.write(string);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (Exception ex) {/*ignore*/}
        }
    }

    public String sprintAttributes(Sprint sprint){
        String fullReport = "\n" +
                "The report for " + sprint.getTitle() +
                "\n\n" + "From " + sprint.getStartDate() + " -- Until " + sprint.getEndDate() +
                "\nWith the following Scrum Master: " + sprint.getScrumMaster().getName() +
                "\nAnd the following Developers: ";
        for(User developer : sprint.getDevelopers()) {
            fullReport = fullReport + developer + "\n";
        };
        return fullReport;
    }
}
