package Domain.DomainServices.Report;

import java.time.LocalDate;

public class Footer {
    private LocalDate date;
    private String version;

    public Footer(LocalDate date, String version){
        this.date = date;
        this.version = version;
    }
}
