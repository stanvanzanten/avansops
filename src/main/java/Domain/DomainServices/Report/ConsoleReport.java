package Domain.DomainServices.Report;

import Domain.DomainModel.Report.Report;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;

public class ConsoleReport extends Report {

    public ConsoleReport(Header header, Footer footer) {
        super(header, footer);
    }

    public void printSprint(Sprint sprint){
        System.out.println(
                sprintAttributes(sprint)
        );
    }

    public String sprintAttributes(Sprint sprint){
        String fullReport = "\n" +
                "The report for " + sprint.getTitle() +
                "\n\n" + "From " + sprint.getStartDate() + " -- Until " + sprint.getEndDate() +
                "\nWith the following Scrum Master: " + sprint.getScrumMaster().getName() +
                "\nAnd the following Developers: ";
                for(User developer : sprint.getDevelopers()) {
                    fullReport = fullReport + developer + "\n";
                };
                return fullReport;
    }
}
