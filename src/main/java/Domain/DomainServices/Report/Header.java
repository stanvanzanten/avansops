package Domain.DomainServices.Report;

public class Header {
    private String logo;
    private String companyName;
    private String projectName;

    public Header(String logo, String companyName, String projectName){
        this.logo = logo;
        this.companyName = companyName;
        this.projectName = projectName;
    }
}
