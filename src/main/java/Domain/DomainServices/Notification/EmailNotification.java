package Domain.DomainServices.Notification;

import Domain.DomainModel.Notification.BodyType;
import Domain.DomainModel.Notification.Notification;
import Domain.DomainModel.User.User;

public class EmailNotification implements Notification {

    @Override
    public void sendNotification(User user, BodyType body) {
        if(body == BodyType.SUCCESS){
            System.out.println(user.getPreference() + " notification to " + user.getName() + ": It was a success!");
        }
        if(body == BodyType.PIPELINE_FAILURE){
            System.out.println(user.getPreference() + " notification to " + user.getName() + ": The pipeline failed, you can try again or make some adjustments.");
        }
        if(body == BodyType.PIPELINE_SUCCESS){
            System.out.println(user.getPreference() + " notification to " + user.getName() + ": The pipeline was successfully released, congrats!");
        }
        if(body == BodyType.READY_FOR_TESTING){
            System.out.println(user.getPreference() + " notification to " + user.getName() + ": A backlog item is ready for testing!");
        }
        if(body == BodyType.YOU_MESSED_UP){
            System.out.println(user.getPreference() + " notification to " + user.getName() + ": Ohh " + user.getName() + "... You've got some work to do!");
        }
        if(body == BodyType.REVIEW_SUCCESS){
            System.out.println(user.getPreference() + " notification to " + user.getName() + ": The sprint has been reviewed properly!");
        }
        if(body == BodyType.REVIEW_FAILURE){
            System.out.println(user.getPreference() + " notification to " + user.getName() + ": The sprint review has failed. You can try again.");
        }
        if(body == BodyType.ITEM_PUT_BACK){
            System.out.println(user.getPreference() + " notification to " + user.getName() + ": An item has been set back to the to do state!");
        }
        if(body == BodyType.NEW_MESSAGE){
            System.out.println(user.getPreference() + " notification to " + user.getName() + ": A new message has been posted in a thread.");
        }
    }
}
