package Domain.DomainServices.Sprint;

import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.Sprint.SprintState;

public class FinishedState implements SprintState {

    Sprint sprint;
    String title = "finished";

    public FinishedState(Sprint sprint) {
        this.sprint = sprint;
    }

    @Override
    public void setInit() {
        System.out.println("The sprint is already done... Don't overdo yourself!");
    }

    @Override
    public void setActive() {
        System.out.println("The sprint is already done... Don't overdo yourself!");
    }

    @Override
    public void setFinished() {
        System.out.println("You already are in the finished state.");
    }

    @Override
    public String getTitle() {
        return title;
    }
}
