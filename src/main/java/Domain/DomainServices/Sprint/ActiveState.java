package Domain.DomainServices.Sprint;

import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.Sprint.SprintState;

public class ActiveState implements SprintState {

    Sprint sprint;
    String title = "active";

    public ActiveState(Sprint sprint){ this.sprint = sprint; }

    @Override
    public void setInit() {
        System.out.println("The sprint has been started already, no way back now...");
    }

    @Override
    public void setActive() {
        System.out.println("The sprint is already in the active state, back to work!");
    }

    @Override
    public void setFinished() {
        System.out.println("Setting the sprint state to finished, nice work!");
        sprint.setState(new FinishedState(this.sprint));
    }
    
    @Override
    public String getTitle() {
        return title;
    }
}
