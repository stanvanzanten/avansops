package Domain.DomainServices.Sprint;

import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.Sprint.SprintState;

public class InitState implements SprintState {

    Sprint sprint;
    String title = "init";

    public InitState(Sprint sprint) { this.sprint = sprint; }

    @Override
    public void setInit() {
        System.out.println("You are already in the init state of the sprint, don't overdo yourself ;)");
    }

    @Override
    public void setActive() {
        System.out.println("The sprint has been set to active, go get 'm!");
        sprint.setState(new ActiveState(this.sprint));
    }

    @Override
    public void setFinished() {
        System.out.println("You can't be finished just yet smarty pants, work first!");
    }

    @Override
    public String getTitle() {
        return title;
    }
}
