package Domain.DomainServices.Pipeline.Analysis;

import Domain.DomainModel.Pipeline.Analysis.Analysis;

public class SonarCube implements Analysis {

    private String title = "Starting SonarCube...";
    private String logs = "Retrieving code...\n" +
            "Scanning...\n" +
            "Found some vulnerabilities...\n" +
            "Found tests...\n" +
            "Retrieving results...\n" +
            " - 14 passing tests\n" +
            " - 6 failing tests\n" +
            " - 52 vulnerabilities\n" +
            " - 73% code coverage\n";
    private String status = "Finished reviewing code.";

    @Override
    public void analyse() {
        System.out.println("\n" + title + "\n");
        System.out.println(logs);
        System.out.println(status);
    }
}
