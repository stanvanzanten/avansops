package Domain.DomainServices.Pipeline.Analysis;

import Domain.DomainModel.Pipeline.Analysis.Analysis;

public class CodeSonar implements Analysis {

    private String title = "Starting CodeSonar...";
    private String logs = "Retrieving code...\n" +
            "Scanning...\n" +
            "Found some vulnerabilities...\n" +
            "Found tests...\n" +
            "Retrieving results...\n" +
            " - 11 passing tests\n" +
            " - 2 failing tests\n" +
            " - 26 vulnerabilities\n" +
            " - 54% code coverage\n";;
    private String status = "Finished reviewing code.";

    @Override
    public void analyse() {
        System.out.println("\n" + title + "\n");
        System.out.println(logs);
        System.out.println(status);
    }
}
