package Domain.DomainServices.Pipeline.Build;

import Domain.DomainModel.Pipeline.Build.Build;

public class Maven implements Build {

    private String title = "Starting Maven Build";
    private String process = "Building project. \n";
    private String status = "Build succeeded!";

    @Override
    public void start() {
        System.out.print(title);
        System.out.print(process);
        System.out.print(status);
    }
}
