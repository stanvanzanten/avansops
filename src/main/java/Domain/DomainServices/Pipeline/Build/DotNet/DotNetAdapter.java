package Domain.DomainServices.Pipeline.Build.DotNet;

import Domain.DomainModel.Pipeline.Build.Build;

public class DotNetAdapter implements Build {

    //By using this adapter the dotnet class still gets to perform it's methods,
    //without inflicting any change on the build interface.
    DotNet dotnet;

    public DotNetAdapter() {
        //initialize a new DotNet class
        this.dotnet = new DotNet();
    }

    @Override
    public void start() {
        //Call the functions from the dotnet class
        dotnet.startBuild();
        dotnet.fullLog();
    }
}
