package Domain.DomainServices.Pipeline.Build.DotNet;

import Domain.DomainModel.Pipeline.Build.Build;

public class DotNet {

    private String title = "Starting Dot Net Build";
    private String process = "Building project. \n";
    private String status = "Build succeeded!";
    private String logs = "Here folows the full build log:\n" +
            "Generating MSBuild file: src/docs/build/logs/TopLib01.csproj...\n" +
            "Generating MSBuild file: src/docs/build/logs/Shared01.csproj...\n" +
            "Generating MSBuild file: src/docs/build/logs/Shared02.csproj...\n" +
            "Generating MSBuild file: src/docs/build/logs/TopLib02.csproj...\n" +
            "Generating MSBuild file: src/docs/build/logs/TopLib01.csproj.nuget.g.props...\n" +
            "Generating MSBuild file: src/docs/build/logs/TopLib02.csproj.nuget.g.props...\n" +
            "Generating MSBuild file: src/docs/build/logs/TopLib01.csproj.nuget.g.targets...\n" +
            "Generating MSBuild file: src/docs/build/logs/TopLib02.csproj.nuget.g.targets...";

    public void startBuild(){
        System.out.println(title);
        System.out.println(process);
    }

    public void fullLog(){
        System.out.println(logs);
        System.out.println(status);
    }
}
