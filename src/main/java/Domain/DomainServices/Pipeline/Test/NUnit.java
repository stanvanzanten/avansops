package Domain.DomainServices.Pipeline.Test;

import Domain.DomainModel.Pipeline.Test.Test;

public class NUnit implements Test {

    private String title = "NUnit test";
    private String result = "Found 22 tests, 18 passing, 4 failing.";
    //Coverage here or in analysis?

    @Override
    public void run() {
        System.out.println("\nStarting the" + title + "\n");
        System.out.println("Starting " + title + " in directory src/tests/NUnit");
        System.out.println("Installing " + title + " plugins in directory src/BuildAgent/Plugins/dotnetPlugin/bin/IntelliJ/BuildServer/NUnitLauncher.exe");
        System.out.println("Start " + title + " Runner");
        System.out.println("Starting " + title + "-2.6.3 under .NET Framework v4.0 x64\n");
        System.out.println(result);
    }
}
