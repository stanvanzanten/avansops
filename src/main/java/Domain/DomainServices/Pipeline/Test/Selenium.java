package Domain.DomainServices.Pipeline.Test;

import Domain.DomainModel.Pipeline.Test.Test;

public class Selenium implements Test {
    private String title = "Selenium test";
    private String result = "Found 36 tests, 33 passing, 3 failing.";
    //Coverage here or in analysis?

    @Override
    public void run() {
        System.out.println("\n" + title + "\n");
        System.out.println("Starting " + title + " in directory src/tests/Selenium");
        System.out.println("Installing " + title + " plugins in directory src/BuildAgent/Plugins/dotnetPlugin/bin/IntelliJ/BuildServer/SeleniumLauncher.exe");
        System.out.println("Start " + title + " Runner");
        System.out.println("Starting " + title + "-4.2.7 under .NET Framework v6.1 x32\n");
        System.out.println(result);
    }
}
