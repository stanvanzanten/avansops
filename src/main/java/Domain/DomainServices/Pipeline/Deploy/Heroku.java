package Domain.DomainServices.Pipeline.Deploy;

import Domain.DomainModel.Pipeline.Deploy.Deploy;

public class Heroku implements Deploy {

    private String title = "Deploying to Heroku...";
    private String status = "Deployed to Heroku in 2m and 48s...";
    private String logs = "Deploying the application to Heroku from repository...\n" +
            "Setting the right server...\n" +
            "Reserving port dedicated server...\n" +
            "Setting values for port...\n" +
            "Deployment started...\n" +
            "Deploying (10%)\n" +
            "Deploying (20%)\n" +
            "Deploying (30%)\n" +
            "Deploying (40%)\n" +
            "Deploying (50%)\n" +
            "Deploying (60%)\n" +
            "Deploying (70%)\n" +
            "Deploying (80%)\n" +
            "Deploying (90%)\n" +
            "Deploying (100%)\n";

    @Override
    public void deploy() {
        System.out.println("\n" + title + "\n");
        System.out.println(logs);
        System.out.println(status);
    }
}
