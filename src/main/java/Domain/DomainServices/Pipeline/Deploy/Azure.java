package Domain.DomainServices.Pipeline.Deploy;

import Domain.DomainModel.Pipeline.Deploy.Deploy;

public class Azure implements Deploy {

    private String title = "Deploying to Azure...";
    private String status = "Deployed to Azure in 1m and 52s...";
    private String logs = "Deploying the application to Azure repo...\n" +
            "Reserving the dedicated port...\n" +
            "Setting values for dedicated port...\n" +
            "Deployment started...\n" +
            "Deploying (10%)\n" +
            "Deploying (20%)\n" +
            "Deploying (30%)\n" +
            "Deploying (40%)\n" +
            "Deploying (50%)\n" +
            "Deploying (60%)\n" +
            "Deploying (70%)\n" +
            "Deploying (80%)\n" +
            "Deploying (90%)\n" +
            "Deploying (100%)\n";

    @Override
    public void deploy() {
        System.out.println(title);
        System.out.println(logs);
        System.out.println(status);
    }
}
