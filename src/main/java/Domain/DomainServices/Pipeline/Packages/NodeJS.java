package Domain.DomainServices.Pipeline.Packages;

import Domain.DomainModel.Pipeline.Packages.Packages;

public class NodeJS implements Packages {

    private String title;
    private String logs;
    private String status;

    public NodeJS() {
        this.title = "Installing NodeJS Packages...";
        this.logs = "installing packages 1/10 (10%)\n" +
                "installing packages 2/10 (20%)\n" +
                "installing packages 3/10 (30%)\n" +
                "installing packages 4/10 (40%)\n" +
                "installing packages 5/10 (50%)\n" +
                "installing packages 6/10 (60%)\n" +
                "installing packages 7/10 (70%)\n" +
                "installing packages 8/10 (80%)\n" +
                "installing packages 9/10 (90%)\n" +
                "installing packages 10/10 (100%)";
        this.status = "NodeJS has been installed.\n";
    }

    @Override
    public void install() {
        System.out.println(title);
        System.out.println(logs);
        System.out.println(status);
    }
}
