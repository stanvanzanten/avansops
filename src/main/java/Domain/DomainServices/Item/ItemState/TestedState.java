package Domain.DomainServices.Item.ItemState;

import Domain.DomainModel.Item.Item;
import Domain.DomainModel.Item.ItemState;

public class TestedState implements ItemState {

    Item item;

    String title = "tested";

    public TestedState(Item item) { this.item = item; }

    @Override
    public void setToDo() {
        System.out.println("You can't go back to the To Do state from here.");
    }

    @Override
    public void setDoing() {
        System.out.println("You can't go back to the Doing state from here.");
    }

    @Override
    public void setReady() {
        System.out.println("Oof that tester better check everything twice this time...");
        item.setState(new ReadyForTestingState(this.item));
    }

    @Override
    public void setToDoFromReady() {
        System.out.println("You can't go back to the to do state from here");
    }

    @Override
    public void setTesting() {
        System.out.println("You can't go back to the Testing state from here.");
    }

    @Override
    public void setTested() {
        System.out.println("You are already testing...");
    }

    @Override
    public void setDone() {
        System.out.println("Well done, good luck on the next one!");
        item.setState(new DoneState(this.item));
    }

    @Override
    public String getTitle() {
        return title;
    }
}
