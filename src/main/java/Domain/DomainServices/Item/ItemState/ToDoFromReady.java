package Domain.DomainServices.Item.ItemState;

import Domain.DomainModel.Item.Item;
import Domain.DomainModel.Item.ItemState;

public class ToDoFromReady implements ItemState {

    Item item;

    String title = "todofromready";

    public ToDoFromReady(Item item) { this.item = item; }

    @Override
    public void setToDo() {
        System.out.println("You already are in the to do state!");
    }

    @Override
    public void setDoing() {
        System.out.println("Setting the state to doing, good luck!");
        item.setState(new DoingState(this.item));
    }

    @Override
    public void setReady() {
        System.out.println("You have to do something first!");
    }

    @Override
    public void setToDoFromReady() {
        System.out.println("You already are in the to do state!");
    }

    @Override
    public void setTesting() {
        System.out.println("You can't start testing from here!");
    }

    @Override
    public void setTested() {
        System.out.println("You don't have anything!");
    }

    @Override
    public void setDone() {
        System.out.println("You can't be done already...");
    }

    @Override
    public String getTitle() {
        return title;
    }
}
