package Domain.DomainServices.Item.ItemState;

import Domain.DomainModel.Item.Item;
import Domain.DomainModel.Item.ItemState;
import Domain.DomainModel.Notification.BodyType;

public class DoingState implements ItemState {

    Item item;
    String title = "doing";

    public DoingState(Item item) { this.item = item; }

    @Override
    public void setToDo() {
        System.out.println("Setting the state back to To Do");
        item.setState(new ToDoState(this.item));
    }

    @Override
    public void setDoing() {
        System.out.println("You already are in the doing state");
    }

    @Override
    public void setReady() {
        System.out.println("Setting the state to ready for testing");
        item.setState(new ReadyForTestingState(this.item));
//        this.item.getDeveloper().getObserver().update(BodyType.READY_FOR_TESTING);
        this.item.getSprint().getTester().getObserver().update(BodyType.READY_FOR_TESTING);
    }

    @Override
    public void setToDoFromReady() {
        System.out.println("You can't go back to that state from here");
    }

    @Override
    public void setTesting() {
        System.out.println("You can't just go to testing, go to ready for testing first");
    }

    @Override
    public void setTested() {
        System.out.println("You can't set the state to tested from here.");
    }

    @Override
    public void setDone() {
        System.out.println("You're not done yet, loads of work to do. Chop chop ;)");
    }

    @Override
    public String getTitle() {
        return title;
    }
}
