package Domain.DomainServices.Item.ItemState;

import Domain.DomainModel.Item.Item;
import Domain.DomainModel.Item.ItemState;

public class DoneState implements ItemState {

    Item item;

    String title = "done";

    public DoneState(Item item) { this.item = item; }

    @Override
    public void setToDo() {
        System.out.println("This item is closed, you can't go back!");
    }

    @Override
    public void setDoing() {
        System.out.println("This item is closed, you can't go back!");
    }

    @Override
    public void setReady() {
        System.out.println("This item is closed, you can't go back!");
    }

    @Override
    public void setToDoFromReady() {
        System.out.println("You can't go back to the to do state from here");
    }

    @Override
    public void setTesting() {
        System.out.println("This item is closed, you can't go back!");
    }

    @Override
    public void setTested() {
        System.out.println("This item is closed, you can't go back!");
    }

    @Override
    public void setDone() {
        System.out.println("This item is closed, you can't go back!");
    }

    @Override
    public String getTitle() {
        return title;
    }
}
