package Domain.DomainServices.Item.ItemState;

import Domain.DomainModel.Item.Item;
import Domain.DomainModel.Item.ItemState;

public class TestingState implements ItemState {

    Item item;

    String title = "testing";

    public TestingState( Item item ) { this.item = item; }

    @Override
    public void setToDo() {
        System.out.println("You can't go back to the to do state from here.");
    }

    @Override
    public void setDoing() {
        System.out.println("You can't go back to the doing state from here.");
    }

    @Override
    public void setReady() {
        System.out.println("You can't go back to the ready for testing state from here.");
    }

    @Override
    public void setToDoFromReady() {
        System.out.println("You can't go back to the to do state from here");
    }

    @Override
    public void setTesting() {
        System.out.println("You already are in the testing state.");
    }

    @Override
    public void setTested() {
        System.out.println("Setting the state to tested");
        item.setState(new TestedState(this.item));
    }

    @Override
    public void setDone() {
        System.out.println("You can't go to the done state from here!");
    }

    @Override
    public String getTitle() {
        return title;
    }
}
