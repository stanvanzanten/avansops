package Domain.DomainServices.Item.ItemState;

import Domain.DomainModel.Item.Item;
import Domain.DomainModel.Item.ItemState;
import Domain.DomainModel.Notification.BodyType;

public class ReadyForTestingState implements ItemState {

    Item item;

    String title = "ready";

    public ReadyForTestingState(Item item) { this.item = item; }

    @Override
    public void setToDo() {
        System.out.println("You can't go back to this state from here.");
    }

    @Override
    public void setDoing() {
        System.out.println("You can not go back to doing from here, message the developer first!");
    }

    @Override
    public void setReady() {
        System.out.println("This item has already been set to ready for testing!");
    }

    @Override
    public void setToDoFromReady() {
        System.out.println("Setting the item back to To do, someone messed up...");
        item.setState(new ToDoFromReady(this.item));
        this.item.getDeveloper().getObserver().update(BodyType.YOU_MESSED_UP);
        this.item.getSprint().getScrumMaster().getObserver().update(BodyType.ITEM_PUT_BACK);
    }

    @Override
    public void setTesting() {
        System.out.println("Setting state to testing, good luck!");
        item.setState(new TestingState(this.item));
    }

    @Override
    public void setTested() {
        System.out.println("You have to test first...");
    }

    @Override
    public void setDone() {
        System.out.println("You haven't done your tests yet! Go do them first.");
    }

    @Override
    public String getTitle() {
        return title;
    }
}
