package Domain.DomainServices.Item;

import Domain.DomainModel.Item.Item;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;

public class Activity extends Item {

    public Activity(String title, String content, User developer, Sprint sprint) {
        super(title, content, developer, sprint);
    }
}
