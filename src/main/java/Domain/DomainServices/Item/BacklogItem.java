package Domain.DomainServices.Item;

import Domain.DomainModel.Item.Item;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;

import java.util.ArrayList;

public class BacklogItem extends Item {
    private ArrayList<Activity> activities;

    public BacklogItem(String title, String content, User developer, Sprint sprint) {
        super(title, content, developer, sprint);
        this.activities = new ArrayList<>();
    }

    //Need to make a function to control if activities are closed before the backlogitem closes.

    public ArrayList<Activity> getActivities() {
        return activities;
    }

    public void addActivity(Activity activity){
        if(super.getState().getTitle().equals("todo") || super.getState().getTitle().equals("doing")){
            this.activities.add(activity);
        } else {
            System.out.println("You cannot add an activity from the " + super.getState().getTitle() + " state");
        }
    }


}
