package Domain.DomainServices.Observers;

import Domain.DomainModel.Notification.BodyType;
import Domain.DomainModel.Notification.NotificationFactory;
import Domain.DomainModel.Observers.Observer;
import Domain.DomainModel.User.User;

public class NotificationObserver implements Observer {

    private User user;
    private NotificationFactory notificationFactory = new NotificationFactory();

    public NotificationObserver(User user) {
        this.user = user;
    }

    @Override
    public void update(BodyType bodyType) {
        //Send notification from factory with the User preference
        notificationFactory.createNotification(user.getPreference()).sendNotification(user, bodyType);
    }
}
