package Domain.DomainModel.Project;

import Domain.DomainModel.Forum.Forum;
import Domain.DomainModel.Observers.Observable;
import Domain.DomainModel.Repository.Repository;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;

import java.util.ArrayList;

public class Project extends Observable {
    private User productOwner;
    private String title;
    private Forum forum;
    private ArrayList<Sprint> sprints;
    private ArrayList<User> contributers;
    private Repository repository;

    public Project(User productOwner, String title) {
        this.productOwner = productOwner;
        this.title = title;
        this.forum = new Forum();
        this.sprints = new ArrayList<>();
        this.contributers = new ArrayList<>();
        this.repository = new Repository();

        subscribe(productOwner.getObserver());
    }

    public void addContributer(User contributer) { this.contributers.add(contributer); }

    public ArrayList<User> getContributers() { return this.contributers; }

    public User getProductOwner() { return this.productOwner; }

    public void addSprint(Sprint sprint){
        //Add the incoming sprint to the sprints list.
        System.out.println("Adding '" + sprint.getTitle() + "' to the project.");
        this.sprints.add(sprint);
        //Add the people working on the sprint to the contributers.
        addContributer(sprint.getScrumMaster());
        //Search the list of developers of the sprint object and add them to the array.
        for(User user : sprint.getDevelopers()) {
            addContributer(user);
        }
    }

    public String getProjectname(){
        return title;
    }

    public ArrayList<Sprint> getSprint() { return this.sprints; }

    public void addRepository(Repository repository){
        this.repository = repository;
    }

    public Repository getRepository() { return this.repository; }

    public Forum getForum() { return this.forum; }
}
