package Domain.DomainModel.Pipeline.Build;

import Domain.DomainServices.Pipeline.Build.*;
import Domain.DomainServices.Pipeline.Build.DotNet.DotNet;
import Domain.DomainServices.Pipeline.Build.DotNet.DotNetAdapter;

public class BuildFactory {

    public Build getBuild(String buildType) {
        //If there is no buildtype, return null so there won't be a build.
        if (buildType == null){
            return null;
        }

        //Check the buildType to return the right build.
        if (buildType.equalsIgnoreCase("dotnet")){
            return new DotNetAdapter();
        }
        else if (buildType.equalsIgnoreCase("dotnetcore")){
            return new DotNetCore();
        }
        else if (buildType.equalsIgnoreCase("maven")){
            return new Maven();
        }
        else if (buildType.equalsIgnoreCase("ant")){
            return new Ant();
        }
        else if (buildType.equalsIgnoreCase("jenkins")){
            return new Jenkins();
        }
        return null;
    }
}
