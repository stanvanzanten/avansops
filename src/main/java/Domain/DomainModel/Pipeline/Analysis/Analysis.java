package Domain.DomainModel.Pipeline.Analysis;

public interface Analysis {
    void analyse();
}
