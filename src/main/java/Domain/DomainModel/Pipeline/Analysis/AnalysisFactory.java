package Domain.DomainModel.Pipeline.Analysis;

import Domain.DomainServices.Pipeline.Analysis.CodeSonar;
import Domain.DomainServices.Pipeline.Analysis.SonarCube;

public class AnalysisFactory {

    public Analysis getAnalysis(String analysisType) {
        if(analysisType == null){
            return null;
        }
        if(analysisType.equalsIgnoreCase("sonarcube")) {
            return new SonarCube();
        } else if(analysisType.equalsIgnoreCase("codesonar")){
            return new CodeSonar();
        }
        return null;
    }
}
