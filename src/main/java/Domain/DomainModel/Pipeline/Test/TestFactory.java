package Domain.DomainModel.Pipeline.Test;

import Domain.DomainServices.Pipeline.Test.NUnit;
import Domain.DomainServices.Pipeline.Test.Selenium;

public class TestFactory {

    public Test getTest(String testType) {
        //If there is no testType, return null so there won't be a test.
        if (testType == null){
            return null;
        }
        if(testType.equalsIgnoreCase("selenium")){
            return new Selenium();
        } else if (testType.equalsIgnoreCase("nunit")){
            return new NUnit();
        }
        return null;
    }
}
