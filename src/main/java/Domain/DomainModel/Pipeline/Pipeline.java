package Domain.DomainModel.Pipeline;

import Domain.DomainModel.Pipeline.Analysis.Analysis;
import Domain.DomainModel.Pipeline.Analysis.AnalysisFactory;
import Domain.DomainModel.Pipeline.Build.Build;
import Domain.DomainModel.Pipeline.Build.BuildFactory;
import Domain.DomainModel.Pipeline.Deploy.Deploy;
import Domain.DomainModel.Pipeline.Deploy.DeployFactory;
import Domain.DomainModel.Pipeline.Packages.Packages;
import Domain.DomainModel.Pipeline.Test.Test;
import Domain.DomainModel.Pipeline.Test.TestFactory;
import Domain.DomainModel.Repository.Repository;

import java.util.ArrayList;
import java.util.Random;

public class Pipeline {

    private Repository source;

    private String buildType;
    private Build build;
    private BuildFactory buildFactory;

    private String testType;
    private Test test;
    private TestFactory testFactory;

    private String deployType;
    private Deploy deploy;
    private DeployFactory deployFactory;

    private String analysisType;
    private Analysis analysis;
    private AnalysisFactory analysisFactory;

    private ArrayList<Packages> packages;

    private boolean finished;
    private boolean failed;
    private boolean reviewed;

    //Need to do the analysis, test and deploy here.

    public Pipeline(String buildType, String testType, String deployType, String analysisType, ArrayList<Packages> packages){

        this.buildFactory = new BuildFactory();
        this.buildType = buildType;
        //Now that the buildType is set, we can retrieve the right build.
        this.build = this.buildFactory.getBuild(this.buildType);

        this.testFactory = new TestFactory();
        this.testType = testType;
        //Now that the testType is set, we can retrieve the right test.
        this.test = this.testFactory.getTest(this.testType);

        this.deployFactory = new DeployFactory();
        this.deployType = deployType;
        //Now that the deployType is set, we can retrieve the right deploy.
        this.deploy = this.deployFactory.getDeploy(this.deployType);

        this.analysisFactory = new AnalysisFactory();
        this.analysisType = analysisType;
        //Now that the analysisType is set, we can retrieve the right analysis.
        this.analysis = this.analysisFactory.getAnalysis(this.analysisType);

        this.packages = packages;
    }

    //Release pipeline, made for deployment sprints. Needs to end with analyse.
    //TODO implement template pattern.
    public final void StartReleasePipeline(){
        System.out.println("\nStarting the release pipeline... \n");
        getSource();
        installPackages();
        startBuild();
        runTests();
        deploy();
        analyse();
        if(!getFailed()){
            this.finished = true;
        } else {
            this.finished = false;
        }
        System.out.println("Finished the pipeline");
    }

    public final void StartReview(){
        System.out.println("\nStarting the review... \n");
        startBuild();
        analyse();
        System.out.println("\nPlease upload the required documents.\n");
        System.out.println("Documents received, thank you.\n");
        if(!getFailed()){
            this.reviewed = true;
        } else {
            this.reviewed = false;
        }
        System.out.println("The sprint has been reviewed!\n");
    }

    //Before we can get the source files we need to initiate the repo, we can do this from the main
    public void setSource(Repository repository){
        this.source = repository;
    }

    public void getSource(){
        System.out.println("Getting the source files.\n");
        source.printFiles();
    }

    public void installPackages(){
        //For each found package in the list a install sequence should be started. So we loop the list first.
        for(Packages toBeInstalled : packages) {
            System.out.println("\nPreparing to install package...\n");
            //For the found package, run the install function. This will return different results for each function.
            toBeInstalled.install();
        }
    }

    public void startBuild(){
        //The only thing that needs to happen is start the build, since the right build already has been selected.
        build.start();
    }

    public void runTests(){
        //The only thing that needs to happen is run the test, since the right test already has been selected.
        test.run();
    }

    public void deploy(){
        //The only thing that needs to happen is deploy the app, since the right deployment technique already has been selected.
        deploy.deploy();
    }

    public void analyse(){
        analysis.analyse();
    }

    public boolean getFinished() { return this.finished; }

    public boolean getReviewed() { return this.reviewed; }

    public boolean getFailed() {
        Random random = new Random();
        int low = 1;
        int high = 100;
        int result = random.nextInt(high-low) + low;
        if(result <= 25){
            this.failed = true;
        } else{
            this.failed = false;
        }
        return failed;
    }
}
