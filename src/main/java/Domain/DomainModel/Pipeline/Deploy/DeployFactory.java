package Domain.DomainModel.Pipeline.Deploy;

import Domain.DomainServices.Pipeline.Deploy.Azure;
import Domain.DomainServices.Pipeline.Deploy.Heroku;

public class DeployFactory {

    public Deploy getDeploy(String deployType) {
        if(deployType == null){
            return null;
        }

        if(deployType.equalsIgnoreCase("azure")){
            return new Azure();
        } else if(deployType.equalsIgnoreCase("heroku")){
            return new Heroku();
        }
        return null;
    }
}
