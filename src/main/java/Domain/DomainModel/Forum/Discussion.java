package Domain.DomainModel.Forum;

import Domain.DomainModel.Notification.BodyType;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Item.BacklogItem;

import java.time.LocalDate;
import java.util.ArrayList;

public class Discussion {

    private BacklogItem backlogItem;
    private ArrayList<Message> messages;
    private LocalDate date;
    private User creator;
    private String description;
    private ArrayList<User> participants;

    public Discussion(BacklogItem backlogItem, User creator, String description){
        this.backlogItem = backlogItem;
        this.creator = creator;
        this.description = description;
        this.date = LocalDate.now();
        this.participants = new ArrayList<>();
        this.messages = new ArrayList<>();
    }

    public void addMessage(Message message){
        if(!this.backlogItem.getState().getTitle().equals("done")){
            this.messages.add(message);
            for(User user :  participants){
                user.getObserver().update(BodyType.NEW_MESSAGE);
            }
        } else {
            System.out.println("The backlog item is closed, you can't discuss about this anymore!");
        }
    }

    public String getDescription() { return description; }

    public ArrayList<Message> getMessages() { return messages; }

    public BacklogItem getBacklogItem() { return backlogItem; }

    public void addParticipant(User user) {
        this.participants.add(user);
    }
}
