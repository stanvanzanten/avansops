package Domain.DomainModel.Forum;

import Domain.DomainModel.User.User;

import java.util.ArrayList;

public class Message {

    private User creator;
    private String text;
    private ArrayList<Message> reactions;

    public Message(User creator, String text){
        this.creator = creator;
        this.text = text;
        this.reactions = new ArrayList<>();
    }

    public String getText() { return text; }

    public void addReaction(Message message){
        this.reactions.add(message);
    }
}
