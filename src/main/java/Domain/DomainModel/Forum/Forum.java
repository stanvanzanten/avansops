package Domain.DomainModel.Forum;

import java.util.ArrayList;

public class Forum {
    private ArrayList<Discussion> discussions;

    public Forum(){
        this.discussions = new ArrayList<>();
    }

    public ArrayList<Discussion> getDiscussions(){ return discussions; }

    public void addDiscussion(Discussion discussion){
        this.discussions.add(discussion);
    }
}
