package Domain.DomainModel.Sprint;

import Domain.DomainModel.Notification.BodyType;
import Domain.DomainModel.Observers.Observable;
import Domain.DomainModel.Pipeline.Pipeline;
import Domain.DomainModel.Report.Report;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Item.BacklogItem;
import Domain.DomainServices.Report.ConsoleReport;
import Domain.DomainServices.Report.Footer;
import Domain.DomainServices.Report.Header;
import Domain.DomainServices.Report.PDFReport;
import Domain.DomainServices.Sprint.FinishedState;
import Domain.DomainServices.Sprint.InitState;

import java.time.LocalDate;
import java.util.ArrayList;

public class Sprint extends Observable {
    private LocalDate startDate;
    private LocalDate endDate;
    private String title;
    private User scrumMaster;
    private User tester;
    private Header header;
    private Footer footer;
    private PDFReport PdfReport;
    private SprintState state;
    private ArrayList<User> developers;
    private ArrayList<BacklogItem> backlogItems;
    private Pipeline pipeline;
    private boolean isRelease;
    private boolean released;
    private boolean reviewed;

    //Maybe add boolean for release sprint or not, this way we can start either the release or normal pipeline.

    public Sprint(String title, User scrumMaster, User tester, Pipeline pipeline, boolean isRelease) {
        this.startDate = LocalDate.now();
        this.endDate = LocalDate.now().plusDays(7);
        this.title = title;
        this.scrumMaster = scrumMaster;
        this.developers = new ArrayList<>();
        this.backlogItems = new ArrayList<>();
        this.tester = tester;
        this.header = new Header("Some logo", "Avans", this.getTitle());
        this.footer = new Footer(LocalDate.now(), "0.1");
        this.PdfReport = new PDFReport(this.header, this.footer);
        state = new InitState(this);
        //Added the pipeline so we can start it from the sprint.
        this.pipeline = pipeline;
        this.isRelease = isRelease;

        //Subscribing the scrumMaster to the observer
        subscribe(scrumMaster.getObserver());
    }

    public User getScrumMaster() { return scrumMaster; }

    public String getTitle() { return title; }

    public LocalDate getStartDate() { return startDate; }

    public LocalDate getEndDate() { return endDate; }

    public User getTester(){
        return tester;
    }

    public ArrayList<User> getDevelopers() { return developers; }

    public void addDeveloper(User developer) { developers.add(developer); }

    public void addBacklogItem(BacklogItem backlogItem) {
        if(this.state.getTitle().equals("init")){
            this.backlogItems.add(backlogItem);
        } else {
            System.out.println("You cannot add any items from this state!");
        }
    }

    public ArrayList<BacklogItem> getBacklogItems(){
        return this.backlogItems;
    }

    public SprintState getState() { return state; }

    public void setState(SprintState state) {
        if(!this.reviewed && !this.released){
            this.state = state;
        }
    }

    public void setInit() {
        if(!this.reviewed && !this.released){
            state.setInit();
        }
    }

    public void setActive() {
        if(!this.reviewed && !this.released){
        state.setActive();
        }
    }

    public void setFinished() {
        if(!this.reviewed && !this.released){
            state.setFinished();
        }
    }

    //For the check within the tests
    public boolean getReleased() { return this.released; }

    public boolean getReviewed() { return this.reviewed; }

    public void startPipeline(){
        //The pipeline can only be started when the sprint is finished, so check the state title.
//        if(this.state.getTitle().equals("finished")){
        if(this.state instanceof FinishedState) {
            if(!this.isRelease) {
//                PdfReport.printSprint(this);
                pipeline.StartReview();
                this.reviewed = true;
                if(this.pipeline.getReviewed()){
                    this.scrumMaster.getObserver().update(BodyType.REVIEW_FAILURE);
                }
                if(!this.pipeline.getReviewed()){
                    this.scrumMaster.getObserver().update(BodyType.REVIEW_SUCCESS);
                }
            } else{
                pipeline.StartReleasePipeline();
                this.released = true;
                if(!this.pipeline.getFinished()){
                    this.scrumMaster.getObserver().update(BodyType.PIPELINE_SUCCESS);
                }
                if(this.pipeline.getFinished()){
                    this.scrumMaster.getObserver().update(BodyType.PIPELINE_FAILURE);
                }
            }
        } else {
            System.out.println("You cannot start the pipeline from this state!");
        }
    }
}
