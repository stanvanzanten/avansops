package Domain.DomainModel.Sprint;

public interface SprintState {

    String title = "";

    void setInit();
    void setActive();
    void setFinished();
    default String getTitle(){
        return title;
    };
}
