package Domain.DomainModel.User;

import Domain.DomainModel.Notification.NotificationType;
import Domain.DomainServices.Observers.NotificationObserver;

public class User {
    private String name;
    private String email;
    private String phone;
    private String password;
    private NotificationType preference;
    private NotificationObserver observer;

    public User(String name, String email, String phone, String password, NotificationType preference){
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.preference = preference;
        this.observer = new NotificationObserver(this);
    }

    public String getName(){ return name; }

    public void setPreference(NotificationType preference){ this.preference = preference; }

    public NotificationType getPreference() { return preference; }

    public NotificationObserver getObserver() { return observer; }
}
