package Domain.DomainModel.Notification;

public enum NotificationType {
    SLACK,
    EMAIL,
    SMS
}
