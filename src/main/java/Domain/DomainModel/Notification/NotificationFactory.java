package Domain.DomainModel.Notification;

import Domain.DomainServices.Notification.EmailNotification;
import Domain.DomainServices.Notification.SlackNotification;
import Domain.DomainServices.Notification.SmsNotification;

public class NotificationFactory {
    public Notification createNotification(NotificationType type){
        if(type == NotificationType.EMAIL) {
            return new EmailNotification();
        }
        if( type == NotificationType.SLACK) {
            return new SlackNotification();
        }
        if( type == NotificationType.SMS) {
            return new SmsNotification();
        }
        return null;
    }
}
