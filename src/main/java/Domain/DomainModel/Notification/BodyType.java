package Domain.DomainModel.Notification;

public enum BodyType {
    SUCCESS,
    READY_FOR_TESTING,
    PIPELINE_SUCCESS,
    PIPELINE_FAILURE,
    YOU_MESSED_UP,
    REVIEW_SUCCESS,
    REVIEW_FAILURE,
    ITEM_PUT_BACK,
    NEW_MESSAGE
}
