package Domain.DomainModel.Notification;

import Domain.DomainModel.User.User;

public interface Notification {
    void sendNotification(User user, BodyType body);
}
