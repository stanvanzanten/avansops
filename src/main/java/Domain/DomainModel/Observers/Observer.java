package Domain.DomainModel.Observers;

import Domain.DomainModel.Notification.BodyType;

public interface Observer {
    void update(BodyType bodyType);
}
