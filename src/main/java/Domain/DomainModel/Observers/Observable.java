package Domain.DomainModel.Observers;

import java.util.ArrayList;

public abstract class Observable {

    private ArrayList<Observer> subscribers = new ArrayList<>();

    public void subscribe(Observer observer) {
        this.subscribers.add(observer);
    }

    public void unsubscribe(Observer observer) {
        this.subscribers.remove(observer);
    }
}
