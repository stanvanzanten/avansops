package Domain.DomainModel.Item;

public interface ItemState {
    String title = "";

    void setToDo();
    void setDoing();
    void setReady();
    void setToDoFromReady();
    void setTesting();
    void setTested();
    void setDone();
    default String getTitle(){
        return title;
    };
}