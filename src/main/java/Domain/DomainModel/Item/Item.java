package Domain.DomainModel.Item;

import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Item.ItemState.ToDoState;

public abstract class Item {
    private String title;
    private String content;
    private User developer;
    private Sprint sprint;
    private ItemState state;

    public Item(String title, String content, User developer, Sprint sprint) {
        this.title = title;
        this.content = content;
        this.developer = developer;
        this.sprint = sprint;
        state = new ToDoState(this);
    }

    public ItemState getState(){
        return state;
    }

    public void setState(ItemState state){
        this.state = state;
    }

    public void setToDo(){
        state.setToDo();
    }

    public void setDoing(){
        state.setDoing();
    }

    public void setReady(){
        state.setReady();
    }

    public void setToDoFromReady(){ state.setToDoFromReady(); }

    public void setTesting(){
        state.setTesting();
    }

    public void setTested(){
        state.setTested();
    }

    public void setDone(){
        state.setDone();
    }

    public User getDeveloper() { return developer; }

    public Sprint getSprint() { return sprint; }
}
