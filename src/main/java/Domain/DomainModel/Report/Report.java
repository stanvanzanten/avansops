package Domain.DomainModel.Report;

import Domain.DomainModel.User.User;
import Domain.DomainServices.Report.Footer;
import Domain.DomainServices.Report.Header;

import java.util.ArrayList;

public abstract class Report {
    private Header header;
    private Footer footer;
    ArrayList<User> team;

    public Report(Header header, Footer footer) {
        this.header = header;
        this.footer = footer;
        this.team = new ArrayList<>();
    }

    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header){
        this.header = header;
    }

    public Footer getFooter() {
        return this.footer;
    }

    public void setFooter(Footer footer){
        this.footer = footer;
    }

}
