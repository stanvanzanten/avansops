package Domain.DomainModel.Repository;

import Domain.DomainModel.User.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Repository {

    private List<String> files;

    public Repository() {
        //Maybe take the files from the current project for fun? Dit it (I think).
        this.files = getFilesFromProject();
    }

    public List<String> getFilesFromProject(){
        try (Stream<Path> walk = Files.walk(Paths.get("./src"))) {
            List<String> result = walk.filter(Files::isRegularFile)
                    .map(x -> x.toString()).collect(Collectors.toList());
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }

    public void printFiles(){
        //Loop through the files and print each one
        for(String file : files){
            System.out.println(file);
        }
    }


}
