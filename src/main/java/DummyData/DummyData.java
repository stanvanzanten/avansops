package DummyData;

import Domain.DomainModel.Forum.Discussion;
import Domain.DomainModel.Forum.Forum;
import Domain.DomainModel.Forum.Message;
import Domain.DomainModel.Notification.NotificationType;
import Domain.DomainModel.Pipeline.Packages.Packages;
import Domain.DomainModel.Pipeline.Pipeline;
import Domain.DomainModel.Project.Project;
import Domain.DomainModel.Repository.Repository;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Item.Activity;
import Domain.DomainServices.Item.BacklogItem;
import Domain.DomainServices.Pipeline.Packages.Express;
import Domain.DomainServices.Pipeline.Packages.NodeJS;

import java.util.ArrayList;

public class DummyData {

    //In the method as below we will return a new project with all the required/featured elements within

    public Project makeProject(){

        //Let's add some users first
        User user1 = new User("Stan van Zanten","stanzanten@hotmail.com","0681443956","Stan", NotificationType.EMAIL);
        User user2 = new User("Joep de Tester","joeptester@hotmail.com","0681443956","Stan", NotificationType.EMAIL);
        User user3 = new User("Sjaak van Developer","sjaakie@hotmail.com","0681443956","Stan", NotificationType.SMS);
        User user4 = new User("Dilivio Chin-A-Teh","dilie_90@hotmail.com","0681443956","Dili", NotificationType.SMS);
        User user5 = new User("Michael de developer","michael@hotmail.com","0681443956","Dili", NotificationType.SLACK);
        User user6 = new User("Piet de niksnut","pietjuh@hotmail.com","0681443956","Dili", NotificationType.SLACK);

        //Now, let's make a new project
        Project project = new Project(user1, "Dummy Project");
        //Add the users to the project
        project.addContributer(user1);
        project.addContributer(user2);
        project.addContributer(user3);
        project.addContributer(user4);
        project.addContributer(user5);
        project.addContributer(user6);


        //Let's make a pipeline that we can link to our sprints later on.
        //Make new packages to send to the pipeline and add them to an ArrayList.
        Packages NodeJS = new NodeJS();
        Packages Express = new Express();
        ArrayList<Packages> packages = new ArrayList<>();
        packages.add(NodeJS);
        packages.add(Express);

        //Initiate repository
        Repository repository = new Repository();

        //Make a new pipeline
        Pipeline pipeline1 = new Pipeline("dotnet", "nunit", "heroku", "sonarcube", packages);
        Pipeline pipeline2 = new Pipeline("maven", "selenium", "azure", "codesonar", packages);
        //Set the pipeline repository(source)
        pipeline1.setSource(project.getRepository());
        pipeline2.setSource(project.getRepository());

        //We want to add 2 sprints to our project, which both have some backlog items
        //The first sprint will initiate a release, the other one will be reviewed.
        Sprint sprint1 = new Sprint("Dummy sprint 1", user4, user2, pipeline1, true);
        Sprint sprint2 = new Sprint("Dummy sprint 1", user4, user2, pipeline2, false);

        //First we make some backlog items and activities.
        BacklogItem backlogItem1 = new BacklogItem("Make something", "I will try to make something here...", user5, sprint1);
        BacklogItem backlogItem2 = new BacklogItem("Fix the thing he wrecked", "I will try to fix everything again...", user6, sprint1);
        Activity activity1 = new Activity("Make something... again!", "It was too much for me alone...", user5, sprint1);
        Activity activity2 = new Activity("Alright, I'll help!", "It was too much for him alone, me to the rescue!", user6, sprint1);

        //Adding the activities to the backlog item
        backlogItem1.addActivity(activity1);
        backlogItem1.addActivity(activity2);

        //Adding the backlog items to the sprints
        sprint1.addBacklogItem(backlogItem1);
        sprint1.addBacklogItem(backlogItem2);
        sprint2.addBacklogItem(backlogItem1);
        sprint2.addBacklogItem(backlogItem2);

        //Add developers to the sprint from the project
        for(User developer: project.getContributers()){
            //Don't add the product owner.
            if(!developer.getName().equals(project.getProductOwner().getName())){
                sprint1.addDeveloper(developer);
                sprint2.addDeveloper(developer);
            }
        }

        //The last thing a project needs is an Forum
        //We will get the forum from the project, so we now it's the right one.
        Forum forum = project.getForum();

        //Lets make a new discussion thread and add some messages to that
        Discussion discussion1 = new Discussion(backlogItem1, user1, "Should we even use AvansOps?");
        Message message1 = new Message(user2, "I think we should, so shut it and start working.");
        Message message2 = new Message(user3, "I think we should, it works perfectly.");
        Message message3 = new Message(user4, "Nha I don't think it works that well.");
        Message message4 = new Message(user3, "Exactly, thank you!!");
        discussion1.addMessage(message1);
        discussion1.addMessage(message2);
        discussion1.addMessage(message3);
        //We add an reaction to an existing message
        message1.addReaction(message4);
        //Add the discussion to the forum
        forum.addDiscussion(discussion1);

        //The project is now full, we can return it!
        return project;
    }
}
