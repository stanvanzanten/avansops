import Domain.DomainModel.Notification.NotificationType;
import Domain.DomainModel.Pipeline.Packages.Packages;
import Domain.DomainModel.Pipeline.Pipeline;
import Domain.DomainModel.Project.Project;
import Domain.DomainModel.Repository.Repository;
import Domain.DomainModel.Sprint.Sprint;
import Domain.DomainModel.User.User;
import Domain.DomainServices.Item.BacklogItem;
import Domain.DomainServices.Item.ItemState.ToDoState;
import Domain.DomainServices.Pipeline.Packages.Express;
import Domain.DomainServices.Pipeline.Packages.NodeJS;
import Domain.DomainServices.Sprint.InitState;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        //Maybe we can make a dummy data folder so we don't have to do it here?

        //Make some new dummy Users
        User user1 = new User("Stan van Zanten","stanzanten@hotmail.com","0681443956","Stan", NotificationType.EMAIL);
        User user2 = new User("Dilivio Chin-A-Teh","dilie_90@hotmail.com","0681443956","Dili", NotificationType.SMS);

        //Make a new project
        Project project = new Project(user1, "My first project!");

        //Make new packages to send to the pipeline and add them to an ArrayList.
        Packages NodeJS = new NodeJS();
        Packages Express = new Express();
        ArrayList<Packages> packages = new ArrayList<>();
        packages.add(NodeJS);
        packages.add(Express);

        //Initiate repository
        Repository repository = new Repository();

        //Make a new pipeline
        Pipeline pipeline = new Pipeline("dotnet", "nunit", "heroku", "sonarcube", packages);
        //Set the pipeline repository(source)
        pipeline.setSource(repository);

        //Initiate a new sprint
        Sprint sprint1 = new Sprint("Sprint week 1", user1, user2, pipeline, true);
        BacklogItem backlogItem1 = new BacklogItem("Make something", "I will try to make something here...", user1, sprint1);
        sprint1.addBacklogItem(backlogItem1);
        backlogItem1.setState(new ToDoState(backlogItem1));
        backlogItem1.setDoing();
        backlogItem1.setReady();

        //Set the sprint states
        sprint1.setInit(); //Not necessary since the sprint is always in the init state at first.
        //In the init state you can add a sprint to a project
        project.addSprint(sprint1);
        sprint1.setActive();
        sprint1.setFinished();
        sprint1.startPipeline();
    }
}
